---
layout: md-wrapper.njk
name: About
---

# About

This site is just as hand-crafted as all the woodworking projects featured within! I've spent a lot of time building (and re-building) a web portfolio for all of my woodworking projects, and this is the latest iteration. During the day I'm a software engineer turned manager for a genetics company. I only moonlight as a woodworker.

Woodworking has been a hobby of mine for years. My creations are almost always my own design (for better or worse). My designs prioritize utility over creative beauty, however the quality workmanship and materials allow beauty to arise organically. My technique is based heavily around old fashioned joinery (as opposed to hardware like screws and nails) and the use of real wood as (opposed to manufactured sheet materials). These techniques make beautiful pieces that are also very strong. My hope is that the build quality and strength of my build and design technique will allow for items to be passed down for generations. Using real wood and doing a lot of joinery by hand means no two
pieces will be exactly the same.


### The tech

Site generated with [Lume](https://lume.land), a sophisticated static site generator. Check out the [source code](https://gitlab.com/freiguy1/ef-woodworking).

