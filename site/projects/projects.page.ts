export const layout = "project.njk";

export default function* ({ projects }) {
  for (const project in projects) {
    yield {
      ...projects[project],
      url: `/projects/${project}`,
    };
  }
}
