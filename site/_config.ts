import lume from "lume/mod.ts";
import nunjucks from "lume/plugins/nunjucks.ts";
import sass from "lume/plugins/sass.ts";
// import { defaults, MarkdownEngine} from "lume/plugins/markdown.ts";
// import loader from "lume/core/loaders/text.ts";
// import { markdownIt, } from "lume/deps/markdown_it.ts";

const site = lume();

site.copy("assets", ".");
site.use(nunjucks());
site.use(sass());

// site.loadData(defaults.extensions, loader, new MarkdownEngine(markdownIt(defaults.options)));

export default site;
